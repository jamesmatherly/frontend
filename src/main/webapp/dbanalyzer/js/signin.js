var rootURL = "rws/services"
var tableData = null;

$(document).ready(function() {
	var request = $.ajax({
		method: 'GET',
		url: rootURL + '/status',
		dataType: "html", // data type of response
                
		success: function (result) {
			$("#login-status").text("Sign in successful. Redirecting...");
			$("#inputEmail").addClass("hidden");
			$("#inputPassword").addClass("hidden");
			$("#remember-me").addClass("hidden");
			$("#signin-button").addClass("hidden");
			setTimeout(function(){
				window.location.replace("tables.html");
			}, 1000);
		}
	});
});

function submittest() {
	fds = $("#signin-form").serialize();
	var request = $.ajax({
		method: 'POST',
		url: rootURL + '/login',
		dataType: "json", // data type of response
		data: fds,
		beforeSend: function (xhr) {
		    xhr.setRequestHeader ("Authorization", "Basic " + btoa($("#inputEmail").val() + ":" + $("#inputPassword").val()));
		},
                
		success: function (result) {
			$("#login-status").text("Sign in successful. Redirecting...");
			$("#inputEmail").prop("disabled", true);
			$("#inputPassword").prop("disabled", true);
			$("#remember-me").prop("disabled", true);
			$("#signin-button").prop("disabled", true);
			
			setTimeout(function(){
				window.location.replace("tables.html");
			}, 1000);
		}
	});
	request.fail(function( jqXHR, textStatus, errorThrown )
    {
        $("#login-status").text("Invalid login credentials")
    });
	$("#signin-form").trigger("reset");
}

$(function() {
	$('.form-control').keypress(function (e) {
		if (e.which == 13) {
			submittest();
			this.blur();
		    return false;
		}
	});
});
