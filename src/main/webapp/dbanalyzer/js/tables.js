var url = "rws/services/";
var tableData = null;
var tableName = null;
var pageNumber = 1;
var pageSize = 25;
var jsonData = null
$(document).ready(function() {
    $('ul.sidebar a').click(function (event){
        event.preventDefault();
        tableName = $(this)[0].id;
        $('ul.sidebar a').each(function(){
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        $.getJSON(url + tableName + '/' + pageNumber + '/' + pageSize, function(data){
            loadTable(data);
        });
    });
    $('#next-btn').click(function(event){
        pageNumber++;
        $('#tableHere table').remove();
        $('ul.sidebar a').each(function(){
            if($(this).hasClass('active')){
                $.getJSON(url + tableName + '/' + pageNumber + '/' + pageSize, function(data){
                    loadTable(data);
                });
            }
        });
    });

    $('#prev-btn').click(function(event){
        pageNumber--;
        $('#tableHere table').remove();
        $('ul.sidebar a').each(function(){
            if($(this).hasClass('active')){
                $.getJSON(url + tableName + '/' + pageNumber + '/' + pageSize, function(data){
                    loadTable(data);
                });
            }
        });
    });
});

function loadTable(data){
            var table = $('<table>');
            var $head = null;
            var $body = $('<tbody>');
            var $nextPage = $('<button>', {type:'button', id:'next-btn'});
            var $prevPage = $('<button>', {type:'button', id:'prev-btn'});
            $nextPage.addClass('btn');
            $nextPage.addClass('btn-primary');
            $prevPage.addClass('btn');
            $prevPage.addClass('btn-primary');
            $nextPage.append('Next');
            $prevPage.append('Previous');
            table.addClass('table');
            table.addClass('table-bordered');
            table.addClass('table-hover');
            $.each(data, function(index, obj) {
                if ($head === null) {
                    // generate the first row if necessary
                    $head = $('<tr>').append($.map(obj, function(value, name) {
                        return $('<th>', { text: name });
                    }));
                    $head.addClass('thead-dark');
                }
                // generate each row
                $('<tr>').append($.map(obj, function(value, name) {
                    return $('<td>', { text: value });
                })).
                    appendTo($body);
            });
            table.append($head).append($body)
            $('#tableHere').append(table);
            $('#paginationHere').append($nextPage);
            $('#paginationHere').append($prevPage);
        }

function logoutService() {
    var request = $.ajax({
        method: 'DELETE',
        url: url + 'murder-token',
                
        success: function (result) {
                window.location.replace("login.jsp");
        }
    });

}