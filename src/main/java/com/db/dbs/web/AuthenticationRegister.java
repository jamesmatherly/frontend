package com.db.dbs.web;

import org.glassfish.jersey.server.ResourceConfig;

 
public class AuthenticationRegister extends ResourceConfig
{
    public AuthenticationRegister()
    {
        packages("com.db.dbs.web");
 
        register(AuthenticationFilter.class);
        System.out.println("<AuthenticationRegister> Filter registered");
    }
}