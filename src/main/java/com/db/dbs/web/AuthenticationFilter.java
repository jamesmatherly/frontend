package com.db.dbs.web;

import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.StringTokenizer;
 
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
 
import org.glassfish.jersey.internal.util.Base64;

import com.db.service.DBService;
 
/**
 * This filter verify the access permissions for a user
 * based on username and passowrd provided in request
 * */
@Provider
public class AuthenticationFilter implements javax.ws.rs.container.ContainerRequestFilter
{
     
    @Context
    private ResourceInfo resourceInfo;
     
    private static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final String AUTHENTICATION_SCHEME = "Basic";
    private static final Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED).build();
    private static final Response ACCESS_FORBIDDEN = Response.status(Response.Status.FORBIDDEN).build();
      
    @Override
    public void filter(ContainerRequestContext requestContext)
    {
        Method method = resourceInfo.getResourceMethod();
        //Access allowed for all
        if( ! method.isAnnotationPresent(PermitAll.class))
        {
        	System.out.println("<AuthenticationFilter> checking permissions");
            //Access denied for all
            if(method.isAnnotationPresent(DenyAll.class))
            {
            	System.out.println("<AuthenticationFilter> DenyAll");
                requestContext.abortWith(ACCESS_FORBIDDEN);
                return;
            }
            
            String encodedUserPassword = "";
            
            // Try token
            Cookie oldToken = requestContext.getCookies().get("token");
            if(oldToken != null) {
            	encodedUserPassword = oldToken.getValue();
            }
            else {
            	//Get request headers
	            final MultivaluedMap<String, String> headers = requestContext.getHeaders();
	              
	            //Fetch authorization header
	            final List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);
	              
	            //If no authorization information present; block access
	            if(authorization == null || authorization.isEmpty())
	            {
	            	try {
	            		requestContext.abortWith(ACCESS_DENIED);
	            	}
	            	catch (IllegalStateException e) {
	            		System.out.println("oops, error but you still can't get in");
	            	}
	            	
	                return;
	            }
	              
	            //Get encoded username and password
	            encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");
            }
            
            //Decode username and password
            final String usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));;
            
            //Split username and password tokens
            final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
            try {
	            final String username = tokenizer.nextToken();
	            final String password = tokenizer.nextToken();
	            //Verify user access
	            if(method.isAnnotationPresent(RolesAllowed.class))
	            {
	                RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
	                Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));
	                  
	                //Is user valid?
	                if( ! isUserAllowed(username, password, rolesSet))
	                {
	                    requestContext.abortWith(ACCESS_DENIED);
	                    return;
	                }
	            }
            }
            catch(NoSuchElementException e) {
            	requestContext.abortWith(ACCESS_DENIED);
                return;
            }
        }
    }
    private boolean isUserAllowed(final String username, final String password, final Set<String> rolesSet)
    {
        boolean isAllowed = false;
          
        //Fetch password from database and match with password in argument
        //If both match then get the defined role for user from database and continue;
        //else return isAllowed [false]
         try {
	        if(DBService.validateUser(username, password))
	        {
	            String userRole = "USER";
	             
	            //Step 2. Verify user role
	            if(rolesSet.contains(userRole))
	            {
	                isAllowed = true;
	                System.out.println("<AuthenticationFilter> permitted");
	            }
	        }
         }
         catch(NullPointerException e) {
        	 System.out.println("<AuthenticationFilter> You're an idiot. Run JSP first.");
         }
         catch(SQLException e) {
        	 e.printStackTrace();
         }
         return isAllowed;
    }
}